Ruby MYSQL Web Server
--
PREREQUISITS:
-
* Ruby 2.0 (possibly might work on ruby 1.9.)
* *nix system (including mac os x). I have not verified this on windows. 

Installation:
-
* Run bundle install to intall needed gems
* Update the app_settings.rb to update settings for web server and database

To Run
-
* `bundle exec ruby api.rb`

View blog at the following URL for details and to ask questions
https://vell.herokuapp.com/blogs/4
or 
ask questions in the issues section on this repository.

NOTICE: 
-
I am not liable and do not guarantee that the code is fit for production. Be sure to test test test that this code works as you want before using it in a production environment. You can distribute and modify the code to your needs.

Testing currently does not work!
-
This is due to me not knowing how to set up cucumber currently for anything other than rails. Once I get that resolved I will write tests.