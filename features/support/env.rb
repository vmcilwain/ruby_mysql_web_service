require_relative File.expand_path('../../../lib/requirements',__FILE__)
 
require "Capybara"
require "Capybara/cucumber"
# require "rspec"

World do
  Capybara.app = "Api"
 
  include Capybara::DSL
  include RSpec::Matchers
end