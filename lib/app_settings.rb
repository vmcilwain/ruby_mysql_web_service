module App
  Settings = OpenStruct.new({
    port_number: 7125,
    db_host: 'localhost',
    db_username: 'foo',
    db_password: 'bar',
    database_name: 'foo_bar_db'
  })
end