class WebServer
  def initialize
    @webserver = TCPServer.new('127.0.0.1', App::Settings.port_number)
  end
  
  def read_file(file)
    File.open(file, 'r')
  end

  def httpd_defaults(session)
    session.print "HTTP/1.1 200/OK\r\nContent-type:text/html\r\n\r\n"
  end

  def start!
    while (session = @webserver.accept)
    	httpd_defaults(session)
    	request = session.gets
    	trimmedrequest = request.gsub(/GET\ \//, '').gsub(/\ HTTP.*/, '')
      data = translate_request(trimmedrequest.chomp)
      display_content(session, data)
      close_session(session)
    end
  end
  
  def translate_request(req)
    case req
  
    when ""
  		read_file("htdocs/index.html")
    when "mysql"
      Mysql.new.query_all
    else
      nil
  	end
  end
  
  def display_content(session, data)
    begin
      if data.class.name == 'Hash'
        session.print JSON(data)
      else
    		session.print data.read() rescue session.print "File not found"
      end
    rescue Errno::ENOENT
      session.print "File not found"
    end
  end
  
  def close_session(session)
    session.close
  end
end