class Mysql
  def initialize
    # @client = Mysql2::Client.new(host: "localhost", username: "root", database: 'tester_development')
    @client = Mysql2::Client.new(host: App::Settings.db_host, 
                                 username: App::Settings.db_username, 
                                 password: App::Settings.db_password, 
                                 database: App::Settings.database_name)
  end
  
  def query_all
    to_hash @client.query "select * from blogs"
  end
  
  def to_hash(results)
    results.each{|row| puts row.inspect}
    h = {}
    results.each do |row| 
      h["#{row['id']}"] = row
    end
    h
  end
end
