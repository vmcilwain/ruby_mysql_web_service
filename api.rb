#!/usr/bin/env ruby

=begin
  Description:
  Ruby webserver to be used as an api serving mysql data via JSON
  
  Requirements:
  Webserver runs on port 7125. This can be changed in the webserver spec below.
  In the app settins module change the default settings to your liking.
  
  Adding additional web pages and data:
  In order to create additional end points, you will need to do the following.
  if you are adding just a web page
    Create your webpage in the htdocs folder
    Add an additional case for that document to be served
      Example: localhost:7125/new_doc = when 'new_doc' then read_file('htdocs/new_doc.html')
  If you are adding an end point
    Create a new query class and inherity from the mysql.rb file.
    Add your queries there and be sure to pass your query to the `to_hash` method
    In the webserver.rb add an additional case for that end point
      Example: localhost:7125/inactive_users = when 'inactive_users' then QueryClass.new.inactive_users
=end


require_relative 'lib/requirements'

WebServer.new.start!